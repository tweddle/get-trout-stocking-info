import sys
import requests
import datetime
import dateutil.parser as dparser
from bs4 import BeautifulSoup

base_url = 'https://dwr.virginia.gov/fishing/trout-stocking-schedule/'
table_name = 'stocking-table'
# https://dwr.virginia.gov/fishing/trout-stocking-schedule/?start_date=November+22%2C+2021&end_date=November+22%2C+2021

def convert_date(datestring):
    # Convert a date string to a python date format
    new_date = dparser.parse(datestring)
    return new_date

def convert_to_dwr_date(date):
    # Convert a python date object to the day/time format
    # expected by the DWR form query
    return date.strftime("%B+%d") + "%2C+" + date.strftime("%Y")

def get_stocking_info(start_date=None, end_date=None):
    if start_date != None:
        start_date = convert_date(start_date)

    if end_date != None:
        end_date = convert_date(end_date)

    if start_date != None and end_date != None:
        # Both start and end dates have values
        if start_date > end_date and start_date != end_date:
            # The dates were passed in the wrong order, flip them
            dateholder = start_date
            start_date = end_date
            end_date = dateholder
        start_date = convert_to_dwr_date(start_date)
        end_date = convert_to_dwr_date(end_date)
        new_url = base_url + '?start_date=' + start_date
        new_url = new_url + '&end_date=' + end_date
    elif start_date != None:
        # if no end date is provided, use it for both dates
        # (return results for a single day)
        start_date = convert_to_dwr_date(start_date)
        new_url = base_url + '?start_date=' + start_date
        new_url = new_url + '&end_date=' + start_date
    else:
        # just grab the default results
        new_url = base_url

    page = requests.get(new_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    table = soup.find(id=table_name)

    tbody = table.find("tbody")
    rows = tbody.findAll("tr")

    stockings = []

    for row in rows:
        cells = row.findAll("td")
        if len(cells) == 5:
            stockings.append({
                'date': cells[0].text,
                'county': cells[1].text,
                'waterbody': cells[2].text,
                'species': cells[4].text
                })

    return stockings

if __name__ == "__main__":
    #print(get_stocking_info('November+22%2C+2021'))
    #print(get_stocking_info('11/20/2021','11/22/2021'))
    if len(sys.argv) == 3:
        # Start and End Dates have been provided
        print(get_stocking_info(sys.argv[1], sys.argv[2]))
    elif len(sys.argv) ==2:
        # Only Start Date provided
        print(get_stocking_info(sys.argv[1]))
    else:
        # no dates provided
        print(get_stocking_info())



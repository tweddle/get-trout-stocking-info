# Details
A python script to pull trout stocking information from the Virginia DWR website

## Example Usage
Get the default stocking report `python troutstocking.py`

Get the stocking report for a specific date `python troutstocking.py "11/18/2021"`

Get the stocking report for a range of dates `python troutstocking.py "11/18/2021" "11/20/2021"`

